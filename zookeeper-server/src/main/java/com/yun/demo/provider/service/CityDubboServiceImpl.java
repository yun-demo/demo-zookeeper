package com.yun.demo.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.yun.demo.api.entity.City;
import com.yun.demo.api.service.CityDubboService;
import org.springframework.stereotype.Component;

/**
 * Created by qingyun.yu on 2018/9/21.
 */
@Service(version = "1.0.0")
@Component
public class CityDubboServiceImpl implements CityDubboService {
    @Override
    public City findCityByName(String cityName) {
        return new City(1L,2L,"温岭","是我的故乡");
    }
}

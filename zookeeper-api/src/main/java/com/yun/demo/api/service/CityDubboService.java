package com.yun.demo.api.service;


import com.yun.demo.api.entity.City;

/**
 * Created by qingyun.yu on 2018/9/21.
 */
public interface CityDubboService {
    City findCityByName(String cityName);
}

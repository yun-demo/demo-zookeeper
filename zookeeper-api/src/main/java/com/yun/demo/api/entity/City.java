package com.yun.demo.api.entity;

import java.io.Serializable;

/**
 * Created by qingyun.yu on 2018/9/21.
 */
public class City implements Serializable{
    private static final long serialVersionUID = 1246571410871375760L;
    private Long id;
    private Long provinceId;
    private String cityName;
    private String description;

    public City(Long id, Long provinceId, String cityName, String description) {
        this.id = id;
        this.provinceId = provinceId;
        this.cityName = cityName;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return String.format("[\"id\": \"%s\", \"provinceId\": \"%s\", \"cityName\": \"%s\", \"description\": \"%s\"]", id, provinceId, cityName, description);
    }
}

package com.yun.demo.consumer.service;

import com.alibaba.dubbo.config.annotation.Reference;

import com.yun.demo.api.entity.City;
import com.yun.demo.api.service.CityDubboService;
import org.springframework.stereotype.Component;

/**
 * Created by qingyun.yu on 2018/9/21.
 */
@Component
public class CityDubboConsumerService {
    @Reference(version = "1.0.0")
    private CityDubboService cityDubboService;

    public void printCity() {
        String cityName = "温岭";
        City city = cityDubboService.findCityByName(cityName);
        System.out.println(city.toString());
    }
}

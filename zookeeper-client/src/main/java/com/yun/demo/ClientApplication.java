package com.yun.demo;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import com.yun.demo.consumer.service.CityDubboConsumerService;

import java.util.concurrent.*;

/**
 * Created by qingyun.yu on 2018/9/21.
 */
//@ImportResource(locations = {"classpath:dubbo-customers.xml"})
@EnableDubboConfiguration
@SpringBootApplication
public class ClientApplication {
    private static final int corePoolSize = 10;
    private static final int maximumPoolSize = 10;
    private static final int keepAliveTime = 10;
    private static final TimeUnit timeUnit = TimeUnit.SECONDS;
    private static final BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>();
    private static final ThreadFactory factory = Executors.defaultThreadFactory();
    private static final RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();
    private static ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, timeUnit, workQueue, factory, handler);

    public static void main(String[] args) {
        // 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
        ConfigurableApplicationContext run = SpringApplication.run(ClientApplication.class, args);

        for(int i = 0; i < corePoolSize; i++) {
            poolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    final CityDubboConsumerService cityService = run.getBean(CityDubboConsumerService.class);

                    cityService.printCity();
                }
            });
        }
    }
}

